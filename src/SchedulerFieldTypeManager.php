<?php

namespace Drupal\scheduler_field;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\scheduler_field\Annotation\SchedulerFieldType;

/**
 * The scheduler field type manager.
 */
class SchedulerFieldTypeManager extends DefaultPluginManager implements SchedulerFieldTypeManagerInterface {

  /**
   * {@inheritDoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SchedulerField/Type', $namespaces, $module_handler,
      SchedulerFieldTypePluginInterface::class,
      SchedulerFieldType::class
    );
    $this->alterInfo('scheduler_field_type_info');
    $this->setCacheBackend($cache_backend, 'scheduler_field_type');
  }

  /**
   * {@inheritDoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return $this->getFactory()->createInstance($plugin_id, $configuration);
  }

}
