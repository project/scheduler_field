<?php

namespace Drupal\scheduler_field;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface implemented by plugin managers.
 *
 * @ingroup plugin_api
 */
interface SchedulerFieldTypeManagerInterface extends PluginManagerInterface {

  /**
   * Returns a pre-configured scheduler field type plugin instance.
   *
   * @param string $plugin_id
   *   The ID of the plugin being instantiated.
   * @param array $configuration
   *   An array of configuration relevant to the plugin instance.
   *
   * @return \Drupal\scheduler_field\SchedulerFieldTypePluginInterface
   *   A schedule field type instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function createInstance($plugin_id, array $configuration = []);

}
