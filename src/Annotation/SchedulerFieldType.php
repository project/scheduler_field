<?php

namespace Drupal\scheduler_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a reusable scheduler field type plugin annotation object.
 *
 * @Annotation
 */
class SchedulerFieldType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the scheduler field type plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * Does the plugin has to be process during cron.
   *
   * @var bool
   */
  public bool $process_during_cron = TRUE;

}
