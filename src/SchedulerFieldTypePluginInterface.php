<?php

namespace Drupal\scheduler_field;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Scheduler field type plugin interface.
 */
interface SchedulerFieldTypePluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Return the name of the reusable form plugin.
   *
   * @return string
   *   The plugin name.
   */
  public function getName(): string;

  /**
   * Process schedule operations.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param \Drupal\Core\Field\FieldItemInterface $field_item
   *   The field items to process.
   */
  public function process(ContentEntityInterface $entity, FieldItemInterface $field_item);

  /**
   * Get entities that need to be processed by scheduler.
   *
   * @return array
   *   An array of entity ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function processScheduler(): array;

  /**
   * Alter query to add specific condition for the current plugin.
   *
   * Each plugin has to define how to get entities to update depending of their
   * purpose.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query to alter.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage.
   * @param \Drupal\field\FieldStorageConfigInterface $field_storage
   *   The field storage.
   */
  public function processSchedulerQuery(SelectInterface $query, EntityStorageInterface $entity_storage, FieldStorageConfigInterface $field_storage): void;

  /**
   * Check if the type is available for a specific entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check availability for.
   *
   * @return bool
   *   TRUE if type is available (default).
   */
  public static function isAvailableForEntity(ContentEntityInterface $entity): bool;

  /**
   * Check if the type is available for entity type.
   *
   * @param string $entityTypeId
   *   The entity type id to check availability for.
   *
   * @return bool
   *   TRUE if type is available (default).
   */
  public static function isAvailableForEntityType(string $entityTypeId): bool;

}
