<?php

namespace Drupal\scheduler_field;

use Drupal\Core\CronInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;

/**
 * The scheduler field cron.
 */
class Cron implements CronInterface {

  /**
   * The scheduler field type manager.
   *
   * @var \Drupal\scheduler_field\SchedulerFieldTypeManager
   */
  protected $schedulerFieldTypeManager;

  /**
   * List of scheduler field types.
   *
   * @var array
   */
  protected array $schedulerFieldTypes;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected QueueInterface $queue;

  /**
   * Cron constructor.
   *
   * @param \Drupal\scheduler_field\SchedulerFieldTypeManager $scheduler_field_type_manager
   *   The scheduler field type manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(SchedulerFieldTypeManager $scheduler_field_type_manager, QueueFactory $queue_factory) {
    $this->schedulerFieldTypeManager = $scheduler_field_type_manager;
    $this->schedulerFieldTypes = $this->schedulerFieldTypeManager->getDefinitions();
    $this->queue = $queue_factory->get('scheduler_field_process');
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function run() {
    $entities = [];
    // For each plugin, generate a query by field storage to get all entities to
    // update depending on each plugin purpose.
    foreach ($this->schedulerFieldTypes as $scheduler_field_type_info) {
      if ($scheduler_field_type_info['process_during_cron']) {
        /** @var \Drupal\scheduler_field\SchedulerFieldTypePluginInterface $scheduler_field_type */
        $scheduler_field_type = $this->schedulerFieldTypeManager->createInstance($scheduler_field_type_info['id']);
        $entities += $scheduler_field_type->processScheduler();
      }
    }

    if (!empty($entities)) {
      foreach (array_chunk($entities, 100, TRUE) as $entities_to_queue) {
        $this->queue->createItem($entities_to_queue);
      }
    }

    return TRUE;
  }

}
