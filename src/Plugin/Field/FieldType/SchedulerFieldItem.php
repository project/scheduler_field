<?php

namespace Drupal\scheduler_field\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the 'scheduler_field' field type.
 *
 * @FieldType(
 *    id = "scheduler_field",
 *    label = @Translation("Scheduler field"),
 *    module = "scheduler_field",
 *    description = @Translation("Schedule tasks like publishing unpublishing
 *    entities."), default_widget = "scheduler_field_default", default_formatter
 *    = "scheduler_field_default", list_class =
 *    "\Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList"
 *  )
 */
class SchedulerFieldItem extends DateRangeItem {

  /**
   * Value for the 'scheduler_type' setting: disabled scheduling (default).
   */
  public const SCHEDULER_TYPE_DISABLED = 'disabled';

  /**
   * Value for the 'scheduler_type' setting: schedule publication.
   */
  public const SCHEDULER_TYPE_PUBLICATION = 'publication';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'scheduler_type' => static::SCHEDULER_TYPE_DISABLED,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['scheduler_type'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Scheduler type'))
      ->setRequired(TRUE);

    // Do not require end value.
    $properties['end_value']->setRequired(FALSE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['scheduler_type'] = [
      'description' => 'The scheduler plugin type.',
      'type' => 'varchar',
      'length' => 255,
    ];

    $schema['indexes']['scheduler_type'] = ['scheduler_type'];

    return $schema;
  }

  /**
   * Get scheduler type plugins as options.
   *
   * @return array
   *   Options for select list with plugin name.
   */
  public static function getSchedulerTypeOptions(?EntityInterface $entity = NULL) {
    $scheduler_field_types = \Drupal::service('plugin.manager.scheduler_field_type')->getDefinitions();
    $options = [];
    /** @var array $scheduler_field_type */
    foreach ($scheduler_field_types as $scheduler_field_type) {
      // Check if type is available for this entity.
      if (NULL !== $entity && $scheduler_field_type['class']::isAvailableForEntity($entity)) {
        $options[$scheduler_field_type['id']] = $scheduler_field_type['name'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $scheduler_field_types = \Drupal::service('plugin.manager.scheduler_field_type')->getDefinitions();
    $options = [];
    /** @var array $scheduler_field_type */
    foreach ($scheduler_field_types as $scheduler_field_type) {
      $options[$scheduler_field_type['id']] = $scheduler_field_type['name'];
    }

    $element['scheduler_type'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Default scheduler type'),
      '#description' => new TranslatableMarkup('Choose the type of the scheduler to use by default.'),
      '#default_value' => $this->getSetting('scheduler_type'),
      '#options' => $options,
      '#disabled' => $has_data,
    ];

    return $element;
  }

}
