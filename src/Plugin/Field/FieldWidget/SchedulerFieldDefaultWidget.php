<?php

namespace Drupal\scheduler_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDefaultWidget;

/**
 * Plugin implementation of the 'scheduler_field_default' widget.
 *
 * @FieldWidget(
 *   id = "scheduler_field_default",
 *   label = @Translation("Scheduler field"),
 *   field_types = {
 *     "scheduler_field"
 *   }
 * )
 */
class SchedulerFieldDefaultWidget extends DateRangeDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show_end_date' => TRUE,
      'show_type_selector' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $element['show_end_date'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the end date field.'),
      '#default_value' => $settings['show_end_date'],
    ];
    $element['show_type_selector'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the schedule type selector.'),
      '#default_value' => $settings['show_type_selector'],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = $this->t('Show end date: @end_date', ['@end_date' => $this->getSetting('show_end_date') ? $this->t('Yes') : $this->t('No')]);
    $summary[] = $this->t('Show schedule type selector: @type', ['@type' => $this->getSetting('show_type_selector') ? $this->t('Yes') : $this->t('No')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $default_type = $items->getFieldDefinition()->getSetting('scheduler_type');

    /** @var \Drupal\scheduler_field\Plugin\Field\FieldType\SchedulerFieldItem $item */
    $item = $items[$delta];
    $element['scheduler_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Scheduler type'),
      '#options' => $item->getSchedulerTypeOptions($items->getEntity()),
      '#default_value' => $item->scheduler_type ?? $default_type,
      '#weight' => -10,
    ];

    $settings = $this->getSettings();
    if (!$settings['show_type_selector']) {
      $element['scheduler_type']['#access'] = FALSE;
    }

    if (!$settings['show_end_date']) {
      $element['end_value']['#access'] = FALSE;
      $element['value']['#title'] = $this->t('Date');
    }

    return $element;
  }

}
