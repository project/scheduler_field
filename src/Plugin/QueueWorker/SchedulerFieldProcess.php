<?php

namespace Drupal\scheduler_field\Plugin\QueueWorker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\scheduler_field\SchedulerFieldTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process schedulers.
 *
 * @QueueWorker(
 *  id = "scheduler_field_process",
 *  title = @Translation("Scheduler field process"),
 *  cron = {"time" = 30},
 * )
 */
class SchedulerFieldProcess extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The scheduler field storages by entity types.
   *
   * @var array
   */
  protected static $schedulerFieldStorage;

  /**
   * The entity type storages.
   *
   * @var array
   */
  protected static $entityTypeStorages;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The scheduler field type.
   *
   * @var \Drupal\scheduler_field\SchedulerFieldTypeManager
   */
  protected $schedulerFieldTypeManager;

  /**
   * Constructs a new SchedulerFieldProcess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\scheduler_field\SchedulerFieldTypeManager $scheduler_field_type_manager
   *   The scheduler field type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, SchedulerFieldTypeManager $scheduler_field_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->schedulerFieldTypeManager = $scheduler_field_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.scheduler_field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entities_type_and_id = [];
    foreach ($data as $key => $value) {
      [$entity_type, $entity_id] = explode(':', $key);
      $entities_type_and_id[$entity_type][$entity_id] = $entity_id;
    }

    foreach ($entities_type_and_id as $entity_type => $entities_id) {
      /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_type_storage */
      $entity_type_storage = $this->getEntityTypeStorage($entity_type);
      $entities = $entity_type_storage->loadMultiple($entities_id);
      if (!empty($entities)) {
        foreach ($entities as $entity) {
          $this->processSchedulerFields($entity);
        }
      }
    }
  }

  /**
   * Process scheduler fields plugins for an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  protected function processSchedulerFields(ContentEntityInterface $entity) {
    $entity_type = $entity->getEntityTypeId();
    $scheduler_fields_storage = self::getSchedulerFieldStorage($entity_type);
    if (!empty($scheduler_fields_storage)) {
      foreach ($scheduler_fields_storage as $scheduler_field_storage) {
        $field_name = $scheduler_field_storage->getName();
        if ($entity->hasField($field_name)) {
          $this->processSchedulerField($entity, $entity->get($field_name));
        }
      }
    }
  }

  /**
   * Process scheduler fields plugins for a field of an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to process.
   * @param \Drupal\Core\Field\FieldItemListInterface $field_items
   *   The field items to process.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function processSchedulerField(ContentEntityInterface $entity, FieldItemListInterface $field_items) {
    /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
    foreach ($field_items as $field_item) {
      $plugin_id = $field_item->get('scheduler_type')->getValue();
      $this->schedulerFieldTypeManager->createInstance($plugin_id)->process($entity, $field_item);
    }
  }

  /**
   * Get scheduler field storages sorted by entity_type.
   *
   * @return array
   *   The scheduler field storages.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getSchedulerFieldStorages(): array {
    if (!empty(static::$schedulerFieldStorage)) {
      return static::$schedulerFieldStorage;
    }

    $scheduler_field_storages = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['field_type' => 'scheduler_field']);
    if (empty($scheduler_field_storages)) {
      return [];
    }

    foreach ($scheduler_field_storages as $scheduler_field_storage) {
      static::$schedulerFieldStorage[$scheduler_field_storage->getTargetEntityTypeId()][$scheduler_field_storage->getName()] = $scheduler_field_storage;
    }

    return static::$schedulerFieldStorage;
  }

  /**
   * Get scheduler fields storage for an entity type.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return array|mixed
   *   The scheduler fields storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function getSchedulerFieldStorage($entity_type) {
    $field_storages = self::getSchedulerFieldStorages();
    return $field_storages[$entity_type] ?? [];
  }

  /**
   * Get entity type storage.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @return mixed
   *   The entity type storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityTypeStorage($entity_type) {
    if (!isset(self::$entityTypeStorages[$entity_type])) {
      self::$entityTypeStorages[$entity_type] = $this->entityTypeManager->getStorage($entity_type);
    }

    return self::$entityTypeStorages[$entity_type];
  }

}
