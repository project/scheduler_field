<?php

namespace Drupal\scheduler_field\Plugin\SchedulerField\Type;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\scheduler_field\SchedulerFieldTypePluginBase;

/**
 * Plugin implementation of the 'Publication' SchedulerFieldType.
 *
 * This plugin change status of entities to publish/unpublish entity changing
 * 'status' property.
 *
 * @SchedulerFieldType(
 *   id = "scheduler_field_type_publication",
 *   name = @Translation("Publication")
 * )
 */
class SchedulerFieldTypePublication extends SchedulerFieldTypePluginBase {

  /**
   * {@inheritDoc}
   */
  public function process(ContentEntityInterface $entity, FieldItemInterface $field_item) {
    $skip = FALSE;
    // If entity is unpublished, check if it has to be published thanks to date
    // values.
    $format = $field_item->getFieldDefinition()->getSetting('datetime_type') === 'datetime' ? 'Y-m-d\TH:i:s' : 'Y-m-d';
    if ($entity->get('status')->getString() === '0') {
      $date_begin = DrupalDateTime::createFromFormat($format, $field_item->get('value')->getValue());
      $date_end_value = $field_item->get('end_value')->getValue();
      $date_end = NULL === $date_end_value ? NULL : DrupalDateTime::createFromFormat($format, $date_end_value);
      $current_date = new DrupalDateTime();
      $current_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      if ($date_begin < $current_date && (NULL === $date_end || $date_end > $current_date)) {
        $entity->set('status', 1);
        $entity->save();
        $skip = TRUE;
      }
    }
    // If entity is published, check if it has to be unpublished thanks to date
    // values.
    if (FALSE === $skip && $entity->get('status')->getString() === '1') {
      $date_end = DrupalDateTime::createFromFormat($format, $field_item->get('end_value')->getValue());
      $current_date = new DrupalDateTime();
      $current_date->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
      if ($date_end < $current_date) {
        $entity->set('status', 0);
        $entity->save();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function processSchedulerQuery(SelectInterface $query, EntityStorageInterface $entity_storage, FieldStorageConfigInterface $field_storage): void {
    $field_name = $field_storage->getName();
    $entity_type_id = $entity_storage->getEntityTypeId();
    $field_table = $this->getFieldTableName($entity_type_id, $field_name);
    $data_table = $entity_storage->getDataTable() ?? $entity_storage->getBaseTable();
    $date_format = ($field_storage->getSetting('datetime_type') === 'datetime') ? 'Y-m-d\TH:i:s' : 'Y-m-d';

    $now = new DrupalDateTime();
    $now->setTimezone(new \DateTimeZone(DateTimeItemInterface::STORAGE_TIMEZONE));
    // Prepare query to filter entities to publish.
    $query_to_publish = $query->andConditionGroup();
    $end_date_condition = $query->orConditionGroup();
    $query_to_publish->condition("$field_table.{$field_name}_value", $now->format($date_format), '<')
      ->condition("$data_table.status", 0);
    $end_date_condition->condition("$field_table.{$field_name}_end_value", $now->format($date_format), '>')
      ->condition("$field_table.{$field_name}_end_value", NULL, 'IS NULL');
    $query_to_publish->condition($end_date_condition);

    // Prepare query fo filter entities to unpublish.
    $query_to_unpublish = $query->andConditionGroup();
    $query_to_unpublish->condition("$field_table.{$field_name}_end_value", $now->format($date_format), '<')
      ->condition("$field_table.{$field_name}_end_value", NULL, 'IS NOT NULL')
      ->condition("$data_table.status", 1);

    // Finally add filters to query.
    $query_or = $query->orConditionGroup();
    $query_or->condition($query_to_publish)
      ->condition($query_to_unpublish);
    $query->condition($query_or);
  }

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntity(ContentEntityInterface $entity): bool {
    return $entity->hasField('status');
  }

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntityType(string $entityTypeId): bool {
    $entity_field_manager = \Drupal::service('entity_field.manager');

    // Get the field definitions for the given entity type.
    $field_definitions = $entity_field_manager->getBaseFieldDefinitions($entityTypeId);

    // Check if the 'status' field exists in the field definitions.
    return isset($field_definitions['status']);
  }

}
