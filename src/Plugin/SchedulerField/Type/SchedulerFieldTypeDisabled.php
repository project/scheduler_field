<?php

namespace Drupal\scheduler_field\Plugin\SchedulerField\Type;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\scheduler_field\SchedulerFieldTypePluginBase;

/**
 * Plugin implementation of the 'Disabled' SchedulerFieldType.
 *
 * This plugin does not provide schedule features.
 *
 * @SchedulerFieldType(
 *   id = "scheduler_field_type_disabled",
 *   name = @Translation("Disabled"),
 *   process_during_cron = FALSE,
 * )
 */
class SchedulerFieldTypeDisabled extends SchedulerFieldTypePluginBase {

  /**
   * {@inheritDoc}
   */
  public function processSchedulerQuery(
    SelectInterface $query,
    EntityStorageInterface $entity_storage,
    FieldStorageConfigInterface $field_storage,
  ): void {}

}
