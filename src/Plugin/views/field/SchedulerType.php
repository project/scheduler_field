<?php

namespace Drupal\scheduler_field\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\scheduler_field\SchedulerFieldTypeManagerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows the scheduler type to be displayed instead of the machine name.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("scheduler_type")
 */
class SchedulerType extends FieldPluginBase {


  /**
   * The scheduler field type manager.
   *
   * @var \Drupal\scheduler_field\SchedulerFieldTypeManagerInterface
   */
  protected $schedulerFieldTypeManager;

  /**
   * Constructs a new Currency object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\scheduler_field\SchedulerFieldTypeManagerInterface $scheduler_field_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SchedulerFieldTypeManagerInterface $scheduler_field_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->schedulerFieldTypeManager = $scheduler_field_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.scheduler_field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['display_name'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['display_name'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display the scheduler type name instead of the machine name'),
      '#default_value' => !empty($this->options['display_name']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    if (!empty($this->options['display_name']) && !empty($value)) {
      $scheduler_field_types = $this->schedulerFieldTypeManager->getDefinitions();
      if (isset($scheduler_field_types[$value])) {
        $value = $scheduler_field_types[$value]['name'];
      }
    }

    return $this->sanitizeValue($value);
  }

}
