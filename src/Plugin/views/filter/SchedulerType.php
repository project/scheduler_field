<?php

namespace Drupal\scheduler_field\Plugin\views\filter;

use Drupal\scheduler_field\SchedulerFieldTypeManagerInterface;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter by scheduler type.
 *
 * @ViewsFilter("scheduler_type")
 */
class SchedulerType extends InOperator {

  /**
   * The scheduler field type manager.
   *
   * @var \Drupal\scheduler_field\SchedulerFieldTypeManagerInterface
   */
  protected $schedulerFieldTypeManager;

  /**
   * Constructs a new Currency object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\scheduler_field\SchedulerFieldTypeManagerInterface $scheduler_field_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SchedulerFieldTypeManagerInterface $scheduler_field_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->schedulerFieldTypeManager = $scheduler_field_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.scheduler_field_type')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $scheduler_field_types = $this->schedulerFieldTypeManager->getDefinitions();
      $options = [];
      /** @var array $scheduler_field_type */
      foreach ($scheduler_field_types as $scheduler_field_type) {
        $options[$scheduler_field_type['id']] = $scheduler_field_type['name'];
      }
      $this->valueOptions = $options;
    }

    return $this->valueOptions;
  }

}
