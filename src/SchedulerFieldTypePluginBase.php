<?php

namespace Drupal\scheduler_field;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\field\FieldStorageConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The scheduler field type plugin base.
 */
abstract class SchedulerFieldTypePluginBase extends PluginBase implements SchedulerFieldTypePluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a ReusableFormPluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritDoc}
   */
  public function process(ContentEntityInterface $entity, FieldItemInterface $field_item) {}

  /**
   * {@inheritDoc}
   */
  public function processScheduler(): array {
    $schedule_entities = [];
    // Get all field storages for scheduler field types so we can next get
    // entity types link to this fields and get all entities that need to be
    // updated.
    $fields_storage = $this->entityTypeManager->getStorage('field_storage_config')->loadByProperties(['type' => 'scheduler_field']);
    /** @var \Drupal\field\FieldStorageConfigInterface $field_storage */
    foreach ($fields_storage as $field_storage) {
      // Act only on available entity types for this scheduler field type.
      if (!static::isAvailableForEntityType($field_storage->getTargetEntityTypeId())) {
        continue;
      }

      $entity_type_id = $field_storage->getTargetEntityTypeId();
      $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);
      $field_name = $field_storage->getName();

      // Get entities to act on.
      $field_table = $this->getFieldTableName($entity_type_id, $field_name);

      // Build a query with select query instead of entity query due to entity
      // query limitations :
      // - For each conditions, entity query join a table.
      // - Does not allow to query on multiple columns for a same table.
      $base_table = $entity_storage->getBaseTable();
      $entity_key = $entity_storage->getEntityType()->getKey('id');
      $data_table = $entity_storage->getDataTable();
      $query = $this->database->select($base_table, $base_table);
      $query->addField($base_table, $entity_key);
      if (!empty($data_table)) {
        $query->join($data_table, $data_table, "$base_table.$entity_key = $data_table.$entity_key");
      }
      $query->join($field_table, $field_table, "$base_table.$entity_key = $field_table.entity_id");
      $query->condition("$field_table.{$field_name}_scheduler_type", $this->getPluginId(), '=');

      $this->processSchedulerQuery($query, $entity_storage, $field_storage);
      $result = $query->execute();

      foreach ($result as $row) {
        $schedule_entities[$entity_type_id . ':' . $row->{$entity_key}] = $row->{$entity_key};
      }
    }

    return $schedule_entities;
  }

  /**
   * {@inheritDoc}
   */
  public function processSchedulerQuery(SelectInterface $query, EntityStorageInterface $entity_storage, FieldStorageConfigInterface $field_name): void {}

  /**
   * Get the table name of an entity field.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $field_name
   *   The machine field name.
   *
   * @return string
   *   The field name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getFieldTableName($entity_type_id, $field_name): string {
    return $this->entityTypeManager->getStorage($entity_type_id)->getTableMapping()->getFieldTableName($field_name);
  }

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntity(ContentEntityInterface $entity): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntityType(string $entityTypeId): bool {
    return TRUE;
  }

}
