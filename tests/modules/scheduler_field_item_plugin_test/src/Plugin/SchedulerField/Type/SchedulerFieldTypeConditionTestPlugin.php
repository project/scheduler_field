<?php

namespace Drupal\scheduler_field_item_plugin_test\Plugin\SchedulerField\Type;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\scheduler_field\SchedulerFieldTypePluginBase;

/**
 * Plugin implementation of the 'condition' SchedulerFieldType for tests.
 *
 * This plugin can be used if a 'changeme' field exists.
 *
 * @SchedulerFieldType(
 *   id = "scheduler_field_type_condition_test",
 *   name = @Translation("Condition test")
 * )
 */
class SchedulerFieldTypeConditionTestPlugin extends SchedulerFieldTypePluginBase {

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntity(ContentEntityInterface $entity): bool {
    return $entity->hasField('changeme');
  }

  /**
   * {@inheritDoc}
   */
  public static function isAvailableForEntityType(string $entityTypeId): bool {
    $entity_field_manager = \Drupal::service('entity_field.manager');

    // Get the field definitions for the given entity type.
    $field_definitions = $entity_field_manager->getBaseFieldDefinitions($entityTypeId);

    // Check if the 'changeme' field exists in the field definitions.
    return isset($field_definitions['changeme']);
  }

}
