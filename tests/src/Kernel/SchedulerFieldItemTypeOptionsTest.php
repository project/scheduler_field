<?php

declare(strict_types=1);

namespace Drupal\Tests\scheduler_field\Kernel;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Test scheduler_field field type via API.
 *
 * @group scheduler_field
 */
class SchedulerFieldItemTypeOptionsTest extends FieldKernelTestBase {

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $fieldNoChangeme;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'text',
    'entity_test',
    'field_test',
    'datetime',
    'datetime_range',
    'scheduler_field',
    'scheduler_field_item_plugin_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a datetime range field.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => strtolower($this->randomMachineName()),
      'entity_type' => 'entity_test',
      'type' => 'scheduler_field',
      'settings' => [
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATE,
        'scheduler_type' => 'scheduler_field_type_disabled',
      ],
      'cardinality' => 1,
    ]);
    $this->fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'entity_test',
      'required' => FALSE,
    ]);
    $this->field->save();

    // Add simple field to test conditions.
    $fieldStorageChangeme = FieldStorageConfig::create([
      'field_name' => 'changeme',
      'entity_type' => 'entity_test',
      'type' => 'string',
    ]);
    $fieldStorageChangeme->save();

    $fieldChangeme = FieldConfig::create([
      'field_storage' => $fieldStorageChangeme,
      'bundle' => 'entity_test',
      'required' => FALSE,
    ]);
    $fieldChangeme->save();

    entity_test_create_bundle('nochangeme');

    $this->fieldNoChangeme = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'nochangeme',
      'required' => FALSE,
    ]);
    $this->fieldNoChangeme->save();

  }

  /**
   * Tests the type option list depending on availability for this entity.
   *
   * The scheduler_field_type_condition_test should be available only for
   * bundles with 'changeme' field.
   */
  public function testTypeOptionsWithChangeme(): void {
    $this->fieldStorage->setSetting('scheduler_type', 'scheduler_field_type_disabled');
    $this->fieldStorage->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE);

    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      $field_name => [
        'scheduler_type' => 'scheduler_field_type_disabled',
      ],
    ]);
    $item = $entity->get($field_name)->first();
    $options = $item->getSchedulerTypeOptions($entity);
    $this->assertContainsEquals('scheduler_field_type_condition_test', array_keys($options));
  }

  /**
   * Tests the type option list depending on availability for this entity.
   *
   * The scheduler_field_type_condition_test should be available only for
   * bundles with 'changeme' field.
   */
  public function testTypeOptionsWithoutChangeme(): void {
    $this->fieldStorage->setSetting('scheduler_type', 'scheduler_field_type_disabled');
    $this->fieldStorage->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE);

    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      'type' => 'nochangeme',
      $field_name => [
        'scheduler_type' => 'scheduler_field_type_disabled',
      ],
    ]);
    $item = $entity->get($field_name)->first();
    $options = $item->getSchedulerTypeOptions($entity);
    $this->assertNotContainsEquals('scheduler_field_type_condition_test', array_keys($options));
  }

}
