<?php

declare(strict_types=1);

namespace Drupal\Tests\scheduler_field\Kernel;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Test scheduler_field field type via API.
 *
 * @group scheduler_field
 */
class SchedulerFieldItemTest extends FieldKernelTestBase {

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'field',
    'text',
    'entity_test',
    'field_test',
    'datetime',
    'datetime_range',
    'scheduler_field',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a datetime range field.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => strtolower($this->randomMachineName()),
      'entity_type' => 'entity_test',
      'type' => 'scheduler_field',
      'settings' => [
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATE,
        'scheduler_type' => 'scheduler_field_type_disabled',
      ],
      'cardinality' => 1,
    ]);
    $this->fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'entity_test',
      'required' => FALSE,
    ]);
    $this->field->save();
  }

  /**
   * Tests the field configured for date-only.
   */
  public function testDateOnly(): void {
    $this->fieldStorage->setSetting('scheduler_type', 'scheduler_field_type_disabled');
    $this->fieldStorage->setSetting('datetime_type', DateTimeItem::DATETIME_TYPE_DATE);
    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $entity = EntityTest::create([
      'name' => $this->randomString(),
      $field_name => [
        'value' => '2016-09-21',
        'end_value' => '2016-09-21',
      ],
    ]);

    // Dates are saved without a time value. When they are converted back into
    // a \Drupal\datetime\DateTimeComputed object they should all have the same
    // time.
    $start_date = $entity->{$field_name}->start_date;
    sleep(1);
    $end_date = $entity->{$field_name}->end_date;
    $this->assertEquals($start_date->getTimestamp(), $end_date->getTimestamp());
    $this->assertEquals('12:00:00', $start_date->format('H:i:s'));
    $this->assertEquals('12:00:00', $end_date->format('H:i:s'));
  }

}
