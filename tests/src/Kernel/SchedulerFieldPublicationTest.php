<?php

declare(strict_types=1);

namespace Drupal\Tests\scheduler_field\Kernel;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\entity_test\Entity\EntityTestRevPub;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Test scheduler_field field type via API.
 *
 * @group scheduler_field
 */
class SchedulerFieldPublicationTest extends EntityKernelTestBase {

  /**
   * A field storage to use in this test class.
   *
   * @var \Drupal\field\Entity\FieldStorageConfig
   */
  protected $fieldStorage;

  /**
   * The field used in this test class.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'scheduler_field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test_revpub');

    // Add a datetime range field.
    $this->fieldStorage = FieldStorageConfig::create([
      'field_name' => strtolower($this->randomMachineName()),
      'entity_type' => 'entity_test_revpub',
      'type' => 'scheduler_field',
      'settings' => [
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATE,
        'scheduler_type' => 'scheduler_field_type_disabled',
      ],
      'cardinality' => 1,
    ]);
    $this->fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $this->fieldStorage,
      'bundle' => 'entity_test_revpub',
      'required' => FALSE,
    ]);
    $this->field->save();

  }

  /**
   * Tests scheduled publication.
   */
  public function testPublication(): void {
    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $entity = EntityTestRevPub::create([
      'name' => $this->randomString(),
      $field_name => [
        'scheduler_type' => 'scheduler_field_type_publication',
        'value' => date('Y-m-d', strtotime('-2 day')),
        'end_value' => date('Y-m-d', strtotime('+1 day')),
      ],
    ]);
    $entity->setUnpublished();
    $entity->save();

    $this->reloadEntity($entity);
    $entity = EntityTestRevPub::load($entity->id());
    $this->assertNotTrue($entity->isPublished());

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(0, $queue->numberOfItems());

    // First cron call put entity in queue.
    \Drupal::service('scheduler_field.cron')->run();

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(1, $queue->numberOfItems());

    // Second cron call unstack queue and process items.
    \Drupal::service('cron')->run();

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(0, $queue->numberOfItems());

    $this->reloadEntity($entity);
    $entity = EntityTestRevPub::load($entity->id());
    $this->assertTrue($entity->isPublished());
  }

  /**
   * Tests scheduled unpublish.
   */
  public function testUnpublish(): void {
    $field_name = $this->fieldStorage->getName();
    // Create an entity.
    $entity = EntityTestRevPub::create([
      'name' => $this->randomString(),
      $field_name => [
        'scheduler_type' => 'scheduler_field_type_publication',
        'value' => date('Y-m-d', strtotime('-10 day')),
        'end_value' => date('Y-m-d', strtotime('-2 day')),
      ],
    ]);
    $entity->setPublished();
    $entity->save();

    $this->reloadEntity($entity);
    $entity = EntityTestRevPub::load($entity->id());
    $this->assertTrue($entity->isPublished());

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(0, $queue->numberOfItems());

    // First cron call put entity in queue.
    \Drupal::service('scheduler_field.cron')->run();

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(1, $queue->numberOfItems());

    // Second cron call unstack queue and process items.
    \Drupal::service('cron')->run();

    $queue = \Drupal::service('queue')->get('scheduler_field_process');
    $this->assertEquals(0, $queue->numberOfItems());

    $this->reloadEntity($entity);
    $entity = EntityTestRevPub::load($entity->id());
    $this->assertNotTrue($entity->isPublished());
  }

}
