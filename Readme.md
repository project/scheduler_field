CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The <strong>scheduler field</strong> module add a scheduler field type.
Scheduler field type allow to configure some schedules actions to fieldable
entities.

The different schedules actions are defined as plugin. By default, two plugins
are available, but you can add your own :

* scheduler_field_type_disabled : This plugin does not provide schedule
  features, used by default to not have scheduled actions.
* scheduler_field_type_publication : This plugin change status of entities to
  publish/unpublish entity changing 'status' property.

Thanks to this plugins, you could add your own plugin to :

* Change a moderation/workflow state
* Change field values
* Send e-mails
* Do whatever you want

Cron is used and has to be called to execute scheduled actions.

The scheduled field module store every details on field table, not
entities.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/scheduler_field

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/scheduler_field


REQUIREMENTS
------------

  * None


INSTALLATION
------------

 * Install this module normally, just like you would install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Workflows > Workflow and
       enable a workflow for the content type.
    3. Navigate to Administration > Structure > Content types >
       [Content type to edit] and add a field of the type "Scheduler field" to
       the node bundle.

Notice: You should run the drupal cron every few minutes to make sure that
updates scheduler updates are executed.


MAINTAINERS
-----------

 * Fabien Clément (Goz) - https://www.drupal.org/u/goz

Supporting organizations:

 * IOSAN - https://www.drupal.org/iosan
